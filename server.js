"use strict";

const express = require("express");
const next = require("next");
const bodyParser = require("body-parser");

const app = next({ dev: false});
const handle = app.getRequestHandler();
const favicon = require('serve-favicon');
const { join } = require('path');

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(bodyParser.urlencoded({extended: true}));
    

    

    server.use('/', require('./router/index'));
 
    server.get("*", (req, res) => {
   
      return handle(req, res);
    });

    server.listen(process.env.PORT || 3000, err => {
      if (err) {
        console.error(err);
      } else {
        console.log("Server ready");
      }
    });
  })
  .catch(ex => {
    console.error(ex);
    process.exit(1);
  });
