const express = require('express');
const router = express.Router();
const toCustomer = require('../src/email/response');
const toSelf = require('../src/email/request')

router.post('/signup', (req, res) =>{
    const {firstName, lastName, email, message} = req.body
    const name = `${firstName} ${lastName}`

    toCustomer(email, name);
    toSelf(name, email, message);

    res.redirect('/contact')
  })

  module.exports = router;