import React, { Component } from "react";
import fetch from 'isomorphic-unfetch';
import '../../styles/style.scss'
class ContactForm extends Component {
  constructor(props){
    super(props);
    this.state={
      firstName: "",
      lastName: "",
      email: "",
      message: ""
    }
  }
onChange(e){
  const keys =`${e.target.name}`;
  const data = e.target.value
  const obj = {};
  obj[keys] = data
  this.setState(obj)
}

  submitForm(){
    const {firstName, lastName, email, message} = this.state
    // e.preventDefault();
    if(err)console.error(error)
    else if(!firstName){
      return null
    }else if(!lastName){
      return null
    }if(!email){
      return null
    }if(!message){
      return null
    }
    else {
      console.log(this.state.firstName)
      return fetch('/signup', {
      method: "post",
      mode: "no-cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      },
      redirect: "/gallery",
      body: JSON.stringify({
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          email: this.state.email,
          message: this.state.message
        })
      
    })}
  }
  render() {
    return (
      <div  id="contact">
      <div id="contactHeader" className="text noMargin " ><h1 id="conH" className="text noMargin underLine">Contact Us</h1><br/>
      <p className="text noMargin">Make an inquiry and we will get back to you as soon as possible.</p>
      </div>
      <div id="contactForm">
        <form action="/signup" method="POST">
        <label htmlFor="" className="text label bold noMargin">First Name*</label ><br/>
          <input onChange={(e)=>this.onChange(e)} value={this.state.firstName} className="Input" type="text" name="firstName" placeholder=" John" required/><br/><br/>
          <label htmlFor="" className="text label bold noMargin">Last Name*</label><br/>
          <input onChange={(e)=>this.onChange(e)} value={this.state.lastName} className="Input" type="text" name="lastName" placeholder=" Doe" required/><br/><br/>
          <label htmlFor="" className="text label bold noMargin">Email Adress*</label >    <br/>   
          <input onChange={(e)=>this.onChange(e)} value={this.state.email} className="Input" type="email" name="email" placeholder=" Example@gmail.com" required/><br/><br/>
          <label htmlFor="" className="text label bold noMargin">Message</label ><br/>
          {/* <div id="messageInp"> */}
          <textarea onChange={(e)=>this.onChange(e)} value={this.state.message} rows="30" className="messageInp" type="text" name="message" placeholder=" Write a message..." required/><br/>
          {/* </div> */}
          <input onClick={this.submitForm} className="btn text" type="submit" name="" value="Submit"  />
        </form>
      </div>
      </div>
    );
  }
}

export default ContactForm;