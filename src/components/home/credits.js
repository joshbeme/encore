export default () => {
  return (
      <div id="credits" className="text">
        Icons made by{" "}
        <a
          className="a text"
          href="https://www.flaticon.com/authors/smartline"
          title="Smartline"
        >
          Smartline
        </a>
        {", "}
        <a
          className="a text"
          href="https://www.flaticon.com/authors/icon-works"
          title="Icon Works"
        >
          Icon Works
        </a>
        {", "}
        <a
          className="a text"
          href="https://www.flaticon.com/authors/simpleicon"
          title="SimpleIcon"
        >
          SimpleIcon
        </a>{" "}
        from{" "}
        <a href="https://www.flaticon.com/" title="Flaticon" className="a text">
          www.flaticon.com
        </a>{" "}
        is licensed by{" "}
        <a
          href="http://creativecommons.org/licenses/by/3.0/"
          title="Creative Commons BY 3.0"
          target="_blank"
          className="a text"
        >
          CC 3.0 BY
        </a>
      </div>
  );
};
