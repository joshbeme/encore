import ScrollAnimation from "react-animate-on-scroll";

export default props => {
  return (
    // <div> <div> <hr/><h1></h1><hr/></div>

    <div className="cardContainer">
    
      <div className="card">
        <img className="svg" src="../static/sketch.svg" />
        <br />
        <ScrollAnimation animateIn="fadeIn">
          <h2 className=" alignCenter noMargin underLine text">Expertise</h2>

          <p className="text">
            Encore can handle all of your General Contracting and Finish
            Carpentry needs with our highly experienced staff, we make your
            project, our passion.
          </p>
        </ScrollAnimation>
      </div>
      <div className="card">
        <img className="svg" src="../static/store.svg" />
        <br />{" "}
        <ScrollAnimation animateIn="fadeIn">
          <h2 className="underLine alignCenter noMargin text">Commercial</h2>
          <p className="text ">
            Completed hundreds of projects on time and under budget over the
            past 25 years for such, fortune 500 companies, as K mart, Sears, and
            many others.
          </p>
        </ScrollAnimation>
      </div>
      <div className="card">
        <img className="svg" src="../static/home.svg" />
        <br />
        <ScrollAnimation animateIn="fadeIn">
          <h2 className=" alignCenter noMargin underLine text">Residential</h2>

          <p className="text">
            Encore has cut its teeth in residential remodels. With our state of
            the art cabinet shop, we can handle all of your renovation and
            custom cabinet needs.
          </p>
        </ScrollAnimation>
      </div>
    </div>

    // </div>
  );
};
