const AboutMain = () => (
  <div id="aboutContainer">
    <h1 className="text underLine" id="aboutHeader">
      ABOUT
    </h1>
    <div id="aboutInfo">
      <img className="aboutImg" id="aboutImg" src="../static/0.jpg" />
      <p className="text" id="aboutParagraph">
        Encore General Contracting was founded by Ed Owens, a licensed general
        contractor with over 25 years of experience in every aspect of
        residential and commercial construction. Never one to shy away from a
        hard day’s work, his career began in demolition operating a jack hammer,
        then developed himself to the point of running multimillion dollar
        commercial projects. Mr. Owens has completed projects for such companies
        as Sears, Kmart, Smart & Final, The Gap, Baby Gap, Chipotle, and
        McDonalds. To seek self-improvement he
        furthered his education and pursued a degree in Civil Engineering
        graduating Magna Cum Laude at California State University of Fullerton. Mr.
        Owen’s greatest passion is to utilize his experience, education, and
        creative skills to bring the vision of his clients to fruition, vd31 in a timely manner and under budget.
      </p>
    </div>
  </div>
);
export default AboutMain;
