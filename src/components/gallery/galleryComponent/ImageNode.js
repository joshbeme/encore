import React, {Component} from "react";
import uuid from 'uuid';

class ImageNode extends Component{
    
    render(){
      const heightWidth = this.props.imageHW;
      const x = this.props.imgX;
      const y = this.props.imgY;
  return (
    <img
    className={`img animate ${this.props.animate} `}
    src={this.props.image}
    style={{
      width: heightWidth,
      height: heightWidth,
      left: x,
      top: y,
      position: "absolute"
     
    }}
    alt=""
  />
  );}
};
export default ImageNode;