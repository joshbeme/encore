const Image =(props)=>{
    return(
        <div className="imageDiv" >
        <img className="galleryImage" src={props.src} />
        </div>
    )
}
export default Image;