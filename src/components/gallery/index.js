import React, { Component, Suspense, lazy } from "react";
import Layout from "../layoutStyle";
import uuid from 'uuid';
import dynamic from 'next/dynamic'
const Image = dynamic(import('./image'))


class Gallery extends Component {
  constructor(props){
    super(props)
    this.state={
      displayDesc: 'none',
      modal: null,
      images: [
        {src: "../static/image-1.jpg", key: uuid()},
        {src: "../static/image-2.jpg", key: uuid()},
        {src: "../static/image-3.jpg", key: uuid()},
        {src: "../static/image-4.jpg", key: uuid()},
        {src: "../static/image-5.jpg", key: uuid()},
        {src: "../static/image-6.jpg", key: uuid()},
        {src: "../static/image-7.jpg", key: uuid()},
        {src: "../static/image-9.jpg", key: uuid()},
        {src: "../static/image-10.jpg", key: uuid()},
        {src: "../static/image-11.jpg", key: uuid()}
        
      ]
    }
    this.enterHandler = this.enterHandler.bind(this);
    this.leaveHandler = this.leaveHandler.bind(this);
  }
  enterHandler(){
    this.setState({
      displayDesc: 'block'
    })
  }
  leaveHandler(){

    this.setState({
      displayDesc: 'none'
    })
  }
  conditionalModal(){
   
    switch(this.props.query.title){
      case 1:
      this.setState({

      })
    }
  }
  componentDidMount(){
    // fetch('https://drive.google.com/drive/u/0/folders/1Pq6ancidQ1Fdwwfbv7c21awEV4VUWcnh')
    // .then(res=>res.json())
    // .then(res=>console.log(res))
    // .catch(res=>console.error(res))
  }
  render() {
    const {displayDesc} = this.state
    return (
      <Layout>
        <div id="mainGallery">
          <div className="galleryHeader header text">
            <h1>GALLERY</h1>
          </div>
          <div id={`galleryContainer`}>
            {this.state.images.map(({src, key})=>{
              return <Image src={src} key={key} ></Image>
            })}
          </div>
        </div>
      </Layout>
    );
  }
}
export default Gallery;
