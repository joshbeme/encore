import Image from './image';
import {withRouter} from 'next/router';
import uuid from 'uuid';

const galleryLinks=[
  {key: uuid(), src:"../static/area1.1.jpg"},
  {key: uuid(), src:"../static/header3.jpg"},
  {key: uuid(), src:"../static/header3.jpg"},
  {key: uuid(), src:"../static/header3.jpg"},
  {key: uuid(), src:"../static/header3.jpg"},

]

export default withRouter((props)=>{

    return(
        
        <div id="mainGallery">
          <div className="galleryHeader text">
            <h1>GALLERY</h1>
          </div>
          <div id={`galleryContainer`}>
         { galleryLinks.map(({key, src}, i)=>{
            return <Image  thumb={src} src={src} keys={key} key={key} />
          })}
 <Modal title={props.router.query.title}/>
          </div>
        </div>
      
    )
})