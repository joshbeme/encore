import Link from 'next/link';
import { prependOnceListener } from 'cluster';

const Gl = (props) => {
return (
    <Link href={`/gallery?title=${props.title}`} title={props.title} >
    {props.children}
    </Link>
)
}
export default Gl