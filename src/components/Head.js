import Head from 'next/head'

export default () => (
  <div>
    <Head>
      <title>Encore GC</title>
      <link href="../styles/style.scss"></link>

      <meta charset="utf-8" key="viewport" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>      
      <meta name="viewport" content="initial-scale=1.0, width=device-width" key="viewport" />
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet"></link>
      <link href="https://fonts.googleapis.com/css?family=Allerta+Stencil" rel="stylesheet"/>
      <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">     
      </link>      
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
      <link rel="stylesheet" href="https://cssicon.space/css/icons.css"/>

    </Head>
  </div>
)