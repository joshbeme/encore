import Credits from "../home/credits";
import Link from "next/link";
import { IoLogoInstagram, IoLogoFacebook, IoLogoLinkedin } from "react-icons/io";

const Footer = props => (
  <div id="footer" className="footer">
  <div id="footerContent"style={{color: "white"}}> 
  <div className="footerSocialMedia" >
  <a className="footerLinks-icons" ><IoLogoLinkedin  className="iconSize" /></a>
  <a className="footerLinks-icons" ><IoLogoInstagram className="iconSize" /></a>
  <a className="footerLinks-icons" ><IoLogoFacebook  className="iconSize" /></a>
  </div>
  <div className="footercontact" >
  <p className="footerContactItem" >Phone: (555)555-5555</p>
  <p className="footerContactItem" >Email: example@gmail.com</p>
  <Link href="/contact">
  <a className="footerLinks footerContactItem" >Contact Us</a>
  </Link>
  </div>
  
  </div>
    <Credits />
  </div>
);

export default Footer;
