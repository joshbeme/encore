import Link from "next/link";
import NavBtn from "./NavLink";
import React, { PureComponent } from "react";
import Footer from './footer';
import Transition from 'react-transition-group/Transition';

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      imgClassName: "",
      left: 0,
      toggleMenu: false,
      in: false,
      display: "none",
      navClass: ""
    };
    
    this.toggleMenu = this.toggleMenu.bind(this);
    this.windowClick = this.windowClick.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
  }

  LeftHandler() {
    if (window.innerWidth > 1300) {
      this.setState({
        left: window.innerWidth / 5 - 65
      });
    } else if (window.innerWidth > 800) {
      this.setState({
        left: window.innerWidth / 10 - 65
      });
    } else {
      this.setState({
        left: 5
      });
    }
  }
  toggleMenu() {
    this.setState({
      toggleMenu: !this.state.toggleMenu,
      display: !this.state.toggleMenu ? "block": setTimeout(()=>"none", 500)
    });
  }
  windowClick(e) {
this.setState({
  toggleMenu: false
})
  }
  handleScroll(){
   if (window.scrollY>0){
    this.setState({
      navClass: "navBarScroll"
    })}
    else{
      this.setState({
        navClass: ""
      })
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, {passive: true})
    this.LeftHandler();
    this.handleScroll()
    document.querySelector("#entireSite").addEventListener("click", this.windowClick);

  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll, {passive: true})
    document.querySelector("#entireSite").removeEventListener("click", this.windowClick);
  }
  render() {
    const {left, toggleMenu, display} = this.state;
    return (
      <div id="navBar" className={`navBar ${this.state.navClass}`}>
        <div
          style={{ left: `${left}px` }}
          id="logoContainer"
          className="logoContainer"
        >
          <Link href="/">
            <a id="imgNavA" className={`imgNav text `}>
              <img
                src="../../static/encore.png"
                className="NavImg"
                id="imgNav"
              />{" "}
              <div className="vl" />
              <p id="imgNavText" className="imgNavText">
                ENCOR<backward>E</backward>
              </p>
            </a>
          </Link>
        </div>

        <div id="navbtns" className="navbtns ">
          <NavBtn name="HOME" class="navbtn" />
          <NavBtn name="ABOUT" class="navbtn" />
          <NavBtn name="GALLERY" class="navbtn" />
          <NavBtn name="CONTACT" class="navbtn" />
        </div>
        <a className="hamburgerA">
          <a
            style={{ position: "relative", width: "100%", height: "100%" }}
            onClick={this.toggleMenu}
          >
            <div
              className={`hamburger icon ${
                toggleMenu ? "remove" : "menu"
              }`}
            />
          </a>
          <div
            id="navBtnsMobile"
            style={{ display: display }}
            className={`animate ${toggleMenu ? "slideOut" : "slideIn"}`}
          >
                      <hr/>
                      <Link href="/">
                      <div>
            <NavBtn name="HOME" class="navbtnMobile "  />
            </div>
            </Link>
            <hr/>
            <Link href="/about">
                      <div >
            <NavBtn name="ABOUT" class="navbtnMobile "  />
            </div>
            </Link>
            <hr/>
            <Link href="/gallery">
                      <div >
            <NavBtn name="GALLERY" class="navbtnMobile "  />
            </div>
            </Link>
            <hr/>
            <Link href="/contact">
                      <div >
            <NavBtn name="CONTACT" class="navbtnMobile "  />
            </div>
            </Link>
            <hr/>
            <Footer/>
          </div>
        </a>
      </div>
    );
  }
}

export default Header;
