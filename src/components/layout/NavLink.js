import Link from "next/link";


function _checkingFor(props){
if (props.name.toLowerCase() === "home"){
return "";
}
else {
    return props.name.toLowerCase();
}
}
export default (props) => (
  <div className={props.class}>
    <Link href={`/${_checkingFor(props)}`} >
      <a className="a text">{props.name}</a>
    </Link>
  
  </div>
);
