import Header from './layout/NavBar'
import Head from './Head'
import Footer from './layout/footer'

const Layout = (props) => (
  <div >
    <Head/>
    <Header />
    <div  id="entireSite">
    {props.children}
    </div>
    <div className="nonMobile" >
    <Footer />
    </div>
  </div>
)

export default Layout