const withSass = require('@zeit/next-sass');
module.exports = withSass({
    sassLoaderOptions:{
        includePaths: ["__dirname" + "/static"]
    },
    postscssLoaderOptions: {
        parser: true
      }
});