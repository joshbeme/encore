import React, {Component} from 'react'
import Layout from "../src/components/layoutStyle";
import GalleryLink from "../src/components/home/galleryLink";
import Cards from "../src/components/home/cards";
import Link from "next/link";

import "../src/styles/style.scss";

class Home extends Component{
constructor(props){
  super(props);
  this.state = {
    imageY: 0
   };
   this.handleScroll = this.handleScroll.bind(this)
}

handleScroll(){
  const box = document.querySelector('.homeHead')
const image = document.querySelector('.homeHeadImg')
console.log(window.scrollY)
  this.setState({
    imageY: window.scrollY
  })
}

  componentDidMount(){
    // window.addEventListener('scroll', this.handleScroll, {passive: true})
  }
  componentWillUnmount(){
    // window.removeEventListener('scroll', this.handleScroll, {passive: true})
  }
  render(){
    return(
  <Layout>
    <div className="HomeLayout text">
      <div className="homeContainer text">
        <div className="homeHead text">
          <img  className="homeHeadImg" src="../static/header3.jpg" />

          <div className="homeImgText">
            <h1 className="headingText text">Welcome to Encore</h1>
            <h5 className="headingText noBold text">
              A general contracting company with years of experience and tons of
              enthusiasm for our craft
            </h5>
          </div>
          <div className="homeImgTextLink">
            <GalleryLink />
          </div>
          <div className="homeHeadTextCont" />
          <img className="inBetween1" src="../static/encore.png" />
        </div>
        <div className="body">
          <div className="cardHeader text alignCenter">
            <h1 className="alignCenter" id="cardHeader">
              WHAT CAN WE DO?
            </h1>
          </div>

          <Cards />
          
        </div>
        <div id="nextImage" >
        <img  className="nextImage" src="../static/header.jpg" />

        </div>
        <div id="homeGallery" className=" backgroundColorHome">
      

          <div
            
            className="homeGalleryTextHeader text  alignCenter"
          >
            <h1 className="alignCenter noBold text noMargin">VIEW OUR WORK</h1>
          </div>

          <div id="homeGalleryContent">
            <img id="homeGalleryImg" src="../static/homeGallery.jpg" />
            <div id="homeGalleryText" className="text ">
              <p className="noMargin">
                After 20 years of experience we have accumulated a numerous
                amount of photos from our projects. If you would like to see
                them, click the button bellow.
              </p>
              <Link href="/gallery">
                <button className="btn text">View Gallery</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Layout>
);}}
export default Home