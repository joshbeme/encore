import Layout from "../src/components/layoutStyle";
import AboutContent from "../src/components/about/index";
import "../src/styles/style.scss";

const About = () => (
  <Layout>
    <div className="about">
      <AboutContent />
    </div>
  </Layout>
);

export default About;
