'use strict'
var gulp = require("gulp"),
  prefixer = require("gulp-autoprefixer"),
  sass = require("gulp-sass"),
  uglifycss = require('gulp-uglifycss'),
  imageMin = require('gulp-imagemin');



gulp.task('image', ()=>{
  return  gulp.src('static/*')
    .pipe(imageMin())
    .pipe(gulp.dest('static/'))
})

gulp.task("style", () => {
  return gulp
    .src("src/styles/*.scss")
   
    .pipe(prefixer())
    
    .pipe(gulp.dest("src/styles/"))
    .on("error", e => console.error(e));
});
